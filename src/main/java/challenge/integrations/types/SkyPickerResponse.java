package challenge.integrations.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SkyPickerResponse {
    private SkyPickerDataObject[] data;

    public SkyPickerDataObject[] getData() {
        return data;
    }

    public String getCityTo() {
        if(data.length > 0) return data[0].getCityTo();
        return "Unknown";
    }

    public void setData(SkyPickerDataObject[] data) {
        this.data = data;
    }
}
