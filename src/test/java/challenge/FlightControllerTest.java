package challenge;

import challenge.controllers.FlightController;
import challenge.models.FlightAverage;
import challenge.services.FlightService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = FlightController.class)
public class FlightControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private FlightService service;



    @Test
    public void whenvalidInput_thenReturns200() throws Exception {
        mockMvc.perform(get("/flight/average?dest=SEN&dest=AOD&dateTo=19/12/2020&dateFrom=09/12/2020&from=OPO&curr=EUR")
        .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void whenWrongDateFormat_thenReturns400() throws Exception {
        mockMvc.perform(get("/flight/average?dest=SEN&dest=AOD&dateTo=50/19/2020&dateFrom=09/12/2020&from=OPO&curr=EUR")
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenNotDefiningParameters_thenReturns400() throws Exception {
        mockMvc.perform(get("/flight/average")
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenInvalidIataCodeFormat_thenReturns400() throws Exception {
        mockMvc.perform(get("/flight/average?dest=SENA&dest=AOD&dateTo=50/19/2020&dateFrom=09/12/2020&from=OPO&curr=EUR")
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenInvalidCurrencyFormat_thenReturns400() throws Exception {
        mockMvc.perform(get("/flight/average?dest=SEN&dest=AOD&dateTo=50/19/2020&dateFrom=09/12/2020&from=OPO&curr=EURO")
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenValidInput_callingServiceLayerProperly() throws Exception {
        when(service.process(anyString(), anyString(), any(Date.class), any(Date.class), anyString())).thenReturn(new FlightAverage("name", "EUR", 10, null, "city"));
        mockMvc.perform(get("/flight/average?dest=SEN&dest=AOD&dateTo=19/12/2020&dateFrom=09/12/2020&from=OPO&curr=EUR")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(
                        content()
                                .json("{\"SEN\":{\"name\":\"name\",\"city\":\"city\",\"currency\":\"EUR\",\"price_average\":10,\"bags_price\":null},\"AOD\":" +
                                        "{\"name\":\"name\",\"city\":\"city\",\"currency\":\"EUR\",\"price_average\":10,\"bags_price\":null}}"));

        Mockito.verify(service, times(2)).process(anyString(), anyString(), any(Date.class), any(Date.class), anyString());
    }
}
