package challenge.validators;

import challenge.annotations.AirportCodeAnnotation;
import challenge.utils.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraintvalidation.SupportedValidationTarget;
import javax.validation.constraintvalidation.ValidationTarget;

@SupportedValidationTarget(ValidationTarget.PARAMETERS)
public class AirportCodeValidator implements ConstraintValidator<AirportCodeAnnotation, Object[]> {
    @Override
    public boolean isValid(Object[] objects, ConstraintValidatorContext constraintValidatorContext) {
        //this validation could be integrated with a service (or database) that provides all airport codes  and validate the airport existence
        //here, we're validating only if the code has 3 non-numeric letters

        //First, we get the "dest" parameter and validate all the destinations
        String[] destinations = (String[]) objects[0];
        for(String dest: destinations) {
            if(dest.length() != 3 || StringUtils.hasNumericChar(dest)) return false;
        }

        //now, we validate the "from" parameter
        String from = (String) objects[1];
        return (from.length() == 3 || !StringUtils.hasNumericChar(from));
    }
}
