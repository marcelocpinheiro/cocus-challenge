package challenge.integrations.types;

public class SkyPickerIntegrationParameters {
    private String flyFrom;
    private String to;
    private String curr;
    private String dateFrom;
    private String dateTo;

    public SkyPickerIntegrationParameters(String flyFrom, String to, String curr, String dateFrom, String dateTo) {
        this.flyFrom = flyFrom;
        this.to = to;
        this.curr = curr;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public String getFlyFrom() {
        return flyFrom;
    }

    public void setFlyFrom(String flyFrom) {
        this.flyFrom = flyFrom;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCurr() {
        return curr;
    }

    public void setCurr(String curr) {
        this.curr = curr;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }
}
