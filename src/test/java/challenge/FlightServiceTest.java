package challenge;

import challenge.database.entities.Airport;
import challenge.database.repositories.AirportRepository;
import challenge.integrations.SkyPickerIntegration;
import challenge.integrations.types.SkyPickerDataObject;
import challenge.integrations.types.SkyPickerIntegrationParameters;
import challenge.integrations.types.SkyPickerResponse;
import challenge.models.FlightAverage;
import challenge.services.FlightService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FlightServiceTest {
    @Mock
    private AirportRepository airportRepository;

    @Mock
    private SkyPickerIntegration integration;

    private FlightService flightService;


    @Test
    public void dealingProperlyWithIntegrationException() {
        when(integration.callService(any(SkyPickerIntegrationParameters.class)))
                .thenThrow(new RuntimeException("teste"));
        flightService = new FlightService(integration, airportRepository);

        FlightAverage fa = flightService.process("test", "test", new Date(), new Date(), "EUR");
        assertThat(fa.getCurrency()).isEqualTo("EUR");
        assertThat(fa.getPrice_average()).isEqualTo(0);
        assertThat(fa.getBags_price()).isEqualTo(null);
    }

    @Test
    public void flightAverageIsReturningProperly() throws JsonProcessingException {
        SkyPickerDataObject dataObject = new SkyPickerDataObject();
        dataObject.setPrice(10.0);
        dataObject.setCityTo("Lisbon");
        dataObject.setBagsPrice(new ObjectMapper().readTree("{\"1\":20,\"2\":40}"));

        SkyPickerDataObject dataObject2 = new SkyPickerDataObject();
        dataObject2.setPrice(30.0);
        dataObject2.setCityTo("Lisbon");
        dataObject2.setBagsPrice(new ObjectMapper().readTree("{\"1\":40,\"2\":80}"));

        SkyPickerDataObject[] dataObjectArray = new SkyPickerDataObject[] { dataObject, dataObject2 };

        SkyPickerResponse response = new SkyPickerResponse();
        response.setData(dataObjectArray);

        Airport airport = new Airport();
        airport.setName("Airport");

        when(airportRepository.findByIataCode(anyString()))
                .thenReturn(List.of(airport));

        when(integration.callService(any(SkyPickerIntegrationParameters.class)))
                .thenReturn(response);

        flightService = new FlightService(integration, airportRepository);

        FlightAverage fa = flightService.process("test", "test", new Date(), new Date(), "EUR");
        assertThat(fa.getCurrency()).isEqualTo("EUR");
        assertThat(fa.getPrice_average()).isEqualTo(20);
        assertThat(fa.getName()).isEqualTo("Airport");
        assertThat(fa.getBags_price().getBag1_average()).isEqualTo(30);
        assertThat(fa.getBags_price().getBag2_average()).isEqualTo(60);
    }
}
