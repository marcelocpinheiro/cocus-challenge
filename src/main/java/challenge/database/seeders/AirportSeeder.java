package challenge.database.seeders;

import challenge.database.entities.Airport;
import challenge.database.repositories.AirportRepository;
import com.opencsv.CSVReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Component
public class AirportSeeder {
    @Autowired
    private AirportRepository repository;

    @EventListener
    public void seed(ContextRefreshedEvent event) {
       try {
           seedAirportTable();
       } catch (Exception e) {
           System.out.println("Couldn't run AirportSeeder: " + e.getMessage());
       }
    }

    private void seedAirportTable() throws IOException {
        if(repository.count() == 0) {
            // reading the csv with the airport data
            Resource resource = new ClassPathResource("data/airports.csv");
            InputStream is = resource.getInputStream();
            try (CSVReader csvReader = new CSVReader(new InputStreamReader(is), ',', '"')) {
                String[] values = null;
                while ((values = csvReader.readNext()) != null) {

                    Airport airport = new Airport(
                            values[0],
                            values[1],
                            values[2],
                            values[3]
                    );

                    System.out.println("Inserting " + airport);
                    repository.save(airport);
                }
            }
        }
    }
}
