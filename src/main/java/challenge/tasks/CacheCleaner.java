package challenge.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class CacheCleaner {

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheCleaner.class);
    private static final long CLEAN_CACHE_INTERVAL = 3600 * 1000;

    @CacheEvict(allEntries = true, cacheNames = { "requests"})
    @Scheduled(fixedRate= CLEAN_CACHE_INTERVAL )
    public void cleanCache() {
        LOGGER.info("Cleaning system cache");
    }
}
