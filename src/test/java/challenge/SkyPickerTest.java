package challenge;

import challenge.integrations.SkyPickerIntegration;
import challenge.integrations.types.SkyPickerDataObject;
import challenge.integrations.types.SkyPickerIntegrationParameters;
import challenge.integrations.types.SkyPickerResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withException;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@RestClientTest(SkyPickerIntegration.class)
public class SkyPickerTest {

    @Autowired
    private MockRestServiceServer server;

    @Autowired
    private SkyPickerIntegration skyPickerIntegration;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void creatingQueryStringProperly() throws JsonProcessingException {

        SkyPickerDataObject dataObject = new SkyPickerDataObject();
        dataObject.setPrice(10.0);
        dataObject.setCityTo("Lisbon");
        dataObject.setBagsPrice(new ObjectMapper().readTree("{\"1\":20,\"2\":40}"));
        SkyPickerDataObject[] dataObjectArray = new SkyPickerDataObject[] { dataObject };

        SkyPickerResponse response = new SkyPickerResponse();
        response.setData(dataObjectArray);

        String detailsString = objectMapper.writeValueAsString(response);

        this.server.expect(requestTo(
                "https://api.skypicker.com/flights?flyFrom=OPO&to=VCP&curr=EUR&dateFrom=09/12/2020&partner=picky&dateTo=19/12/2020&select_airlines=TP,FR"))
                .andRespond(withSuccess(detailsString, MediaType.APPLICATION_JSON));

        SkyPickerIntegrationParameters params = new SkyPickerIntegrationParameters("OPO", "VCP", "EUR", "09/12/2020", "19/12/2020");
        SkyPickerResponse integrationResponse = skyPickerIntegration.callService(params);

        assertThat(integrationResponse).isNotNull();
        assertThat(integrationResponse.getCityTo()).isEqualTo("Lisbon");
        this.server.verify();
    }

    @Test
    public void dealingWithExceptionsProperly() throws JsonProcessingException {

        this.server.expect(requestTo(
                "https://api.skypicker.com/flights?flyFrom=OPO&to=VCP&curr=EUR&dateFrom=09/12/2020&partner=picky&dateTo=19/12/2020&select_airlines=TP,FR"))
                .andRespond(withException(null));

        SkyPickerIntegrationParameters params = new SkyPickerIntegrationParameters("OPO", "VCP", "EUR", "09/12/2020", "19/12/2020");
        Exception exception = assertThrows(RuntimeException.class, () -> {
            SkyPickerResponse integrationResponse = skyPickerIntegration.callService(params);
        });

        this.server.verify();
    }

}
