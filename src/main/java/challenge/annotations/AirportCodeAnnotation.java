package challenge.annotations;

import challenge.validators.AirportCodeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = AirportCodeValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface AirportCodeAnnotation {

    String message() default
            "Airport code must contain 3 non numeric letters";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
