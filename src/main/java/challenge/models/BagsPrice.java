package challenge.models;

public class BagsPrice {
    private long bag1_average;
    private long bag2_average;

    public BagsPrice(long bag1_average, long bag2_average) {
        this.bag1_average = bag1_average;
        this.bag2_average = bag2_average;
    }

    public long getBag1_average() {
        return bag1_average;
    }

    public long getBag2_average() {
        return bag2_average;
    }
}
