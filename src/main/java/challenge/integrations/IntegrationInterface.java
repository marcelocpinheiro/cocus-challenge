package challenge.integrations;

public interface IntegrationInterface<Returns, Parameters> {
    Returns callService(Parameters parameters);
}
