package challenge.database.repositories;

import challenge.database.entities.Airport;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface AirportRepository extends CrudRepository<Airport, Long> {
    public List<Airport> findByIataCode(String iataCode);
}
