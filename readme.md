# Cocus Java Challenge

### Overview 
The company Flight-In-Portugal needs a backend API to provide to external usage. What we are expecting is a new API that provides flights information between Oporto -> Lisbon or Lisbon -> Oporto from the company TAP and Ryanair. The API should provide a calculation of the price average(avg) of the total flights and can be filtered by destination and dates, besides that, the API should know the currency to be used on the conversion of the price.
***

### Setup

* Run the command `docker-compose up -d` to create the database container
* Run the command `./mvnw spring-boot:run` on the application root to init the application

### Tests
* To run the tests, just run the command `./mvnw test` on the application root


### Consuming the API

Here's an example of how to consume the api. Just run the command below.

```
curl --location --request GET 'http://{APPLICATION_HOST}/flight/average?dest=LIS&dest=OPO&dateFrom=01/01/2020&dateTo=01/01/2021&curr=GBP&from=VCP' \
--header 'Content-Type: application/json'
```

if you're running locally, the `APPLICATION_HOST` variable must be `http://localhost:8080`. Otherwise, it will be your server's host.

**IMPORTANT**: You need to specify the `Content-Type: application/json'` header, otherwise, the api will return an error.
***

### Describing the query-string parameters

#### Destinations (dest)
This parameter specifies de IATA Codes of the destination airports that will be used to calculate the average prices. You can specify as many airports as you want, just replicate the parameter. For example: 
```
http://{APPLICATION_HOST}/flight/average?dest=LIS&dest=OPO&dest=CGH
```
This parameter is Required.


#### Origin (from)
This parameter specifies de IATA Code of the origin airport that will be used to calculate the average prices. You can specify only one origin per request. For example: 
```
http://{APPLICATION_HOST}/flight/average?from=VCP
```
This parameter is Required.

#### From date (dateFrom)
This parameter specifies de initial date of the interval of dates to search for flights price. the format is DD/MM/YYYY. Here's an example
```
http://{APPLICATION_HOST}/flight/average?dateFrom=18/10/2020
```
This parameter is Optional.

#### To date (dateTo)
This parameter specifies de end date of the interval of dates to search for flights price. the format is DD/MM/YYYY. Here's an example
```
http://{APPLICATION_HOST}/flight/average?dateTo=18/10/2020
```
This parameter is Optional.

#### Currency (curr)
This parameter specifies currency that the prices must be showed. The format is the ISO 4217 pattern (EUR, USD, BRL, etc).
If you don't specify the currency, the api will return the prices in EUR values.
```
http://{APPLICATION_HOST}/flight/average?curr=USD
```
This parameter is Optional.
***
### How it works?
This apis consume data from the https://www.kiwi.com/ database (you can find the documentation in https://docs.kiwi.com/) and calculate the average price by iterating over the flights.

### Directory structure
```
├── src
│   ├── main
│   │   ├── java
│   │   │   └── challenge
│   │   │       ├── annotations
│   │   │       │   └── AirportCodeAnnotation.java
│   │   │       ├── ChallengeApplication.java
│   │   │       ├── configurations
│   │   │       │   └── CacheConfiguration.java
│   │   │       ├── controllers
│   │   │       │   └── FlightController.java
│   │   │       ├── database
│   │   │       │   ├── entities
│   │   │       │   │   └── Airport.java
│   │   │       │   ├── repositories
│   │   │       │   │   └── AirportRepository.java
│   │   │       │   └── seeders
│   │   │       │       └── AirportSeeder.java
│   │   │       ├── integrations
│   │   │       │   ├── IntegrationInterface.java
│   │   │       │   ├── SkyPickerIntegration.java
│   │   │       │   └── types
│   │   │       │       ├── SkyPickerDataObject.java
│   │   │       │       ├── SkyPickerIntegrationParameters.java
│   │   │       │       └── SkyPickerResponse.java
│   │   │       ├── models
│   │   │       │   ├── BagsPrice.java
│   │   │       │   └── FlightAverage.java
│   │   │       ├── services
│   │   │       │   └── FlightService.java
│   │   │       ├── tasks
│   │   │       │   └── CacheCleaner.java
│   │   │       ├── utils
│   │   │       │   └── StringUtils.java
│   │   │       └── validators
│   │   │           └── AirportCodeValidator.java
│   │   └── resources
│   │       ├── application.properties
│   │       ├── data
│   │       │   └── airports.csv
│   │       └── db
│   │           └── changelog
│   │               └── db.changelog-master.xml
│   └── test
│       └── java
│           └── challenge
│               ├── FlightControllerTest.java
│               ├── FlightServiceTest.java
│               ├── MainFlowIntegrationTest.java
│               └── SkyPickerTest.java
```

### Contact
If you have any issues, doubts, feedbacks, suggestion or anything else, just contact me:
* email: marcelompinheiro@outlook.com
* twitter: @celomamp


Made with love by Marcelo (wanting to be part of Cocus team :D)


