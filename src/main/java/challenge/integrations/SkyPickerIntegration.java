package challenge.integrations;

import challenge.integrations.types.SkyPickerIntegrationParameters;
import challenge.integrations.types.SkyPickerResponse;
import challenge.services.FlightService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Date;

@Component("skypicker")
public class SkyPickerIntegration implements IntegrationInterface<SkyPickerResponse, SkyPickerIntegrationParameters> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SkyPickerIntegration.class);

    private RestTemplate rt;

    public SkyPickerIntegration(RestTemplateBuilder builder) {
        this.rt = builder.build();
    }

    @Value("${configuration.airlines:FR,TP}")
    private String airlines;

    @Value("${configuration.host:https://api.skypicker.com}")
    private String apiHost;

    @Override
    public SkyPickerResponse callService(SkyPickerIntegrationParameters parameters) {
        StringBuilder qs = new StringBuilder();
        qs.append("flyFrom=").append(parameters.getFlyFrom().toUpperCase()).append("&")
                .append("to=").append(parameters.getTo().toUpperCase()).append("&")
                .append("curr=").append(parameters.getCurr()).append("&")
                .append("partner=").append("picky").append("&")
                .append("select_airlines=").append(this.airlines);

        if(parameters.getDateFrom() != null) qs.append("&dateFrom=").append(parameters.getDateFrom());
        if(parameters.getDateTo() != null) qs.append("&dateTo=").append(parameters.getDateTo());

        StringBuilder url = new StringBuilder();
        url.append(apiHost).append("/flights?").append(qs.toString());

        String completeUrl = url.toString();

        LOGGER.info("Requesting data from skypicker API. Request definition: {}",  completeUrl);

        try {
            ResponseEntity<SkyPickerResponse> response = this.rt.exchange(completeUrl, HttpMethod.GET, null, SkyPickerResponse.class);
            return response.getBody();
        } catch (Exception e) {
            String message = "Fail request to skypicker. Message: " + e.getMessage();
            LOGGER.error(message);
            throw new RuntimeException(message);
        }
    }
}
