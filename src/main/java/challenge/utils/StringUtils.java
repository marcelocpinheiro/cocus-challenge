package challenge.utils;

public class StringUtils {

    public static boolean hasNumericChar(String value) {
        char[] chars = value.toCharArray();
        for(char c : chars) {
            if(Character.isDigit(c)) return true;
        }
        return false;
    }
}
