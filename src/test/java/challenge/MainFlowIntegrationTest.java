package challenge;

import challenge.database.entities.Airport;
import challenge.database.repositories.AirportRepository;
import challenge.integrations.SkyPickerIntegration;
import challenge.integrations.types.SkyPickerDataObject;
import challenge.integrations.types.SkyPickerResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureMockRestServiceServer;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureMockRestServiceServer
public class MainFlowIntegrationTest {
    @Autowired
    private MockRestServiceServer server;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AirportRepository repository;

    @Before
    public void setup() throws JsonProcessingException {
        SkyPickerDataObject dataObject = new SkyPickerDataObject();
        dataObject.setPrice(10.0);
        dataObject.setCityTo("Lisbon");
        dataObject.setBagsPrice(new ObjectMapper().readTree("{\"1\":20,\"2\":40}"));
        SkyPickerDataObject[] dataObjectArray = new SkyPickerDataObject[] { dataObject };

        SkyPickerResponse response = new SkyPickerResponse();
        response.setData(dataObjectArray);

        String detailsString = objectMapper.writeValueAsString(response);

        this.server.expect(requestTo(
                "https://api.skypicker.com/flights?flyFrom=OPO&to=VCP&curr=EUR&partner=picky&select_airlines=TP,FR&dateFrom=09/12/2020&dateTo=19/12/2020"))
                .andRespond(withSuccess(detailsString, MediaType.APPLICATION_JSON));

        Airport airport = new Airport();
        airport.setName("Test");
        when(repository.findByIataCode(anyString())).thenReturn(List.of(airport));
    }

    @Test
    public void testEntireFlow() throws Exception {
        mockMvc.perform(get("/flight/average?dest=VCP&dateTo=19/12/2020&dateFrom=09/12/2020&from=OPO&curr=EUR")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"VCP\":{\"name\":\"Test\",\"city\":\"Lisbon\",\"currency\":\"EUR\",\"price_average\":10,\"bags_price\":{" +
                        "\"bag1_average\":20,\"bag2_average\":40}}}\""));
        this.server.verify();
    }


}
