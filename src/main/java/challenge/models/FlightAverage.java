package challenge.models;

public class FlightAverage {
    private String name;
    private String city;
    private String currency;
    private long price_average;
    private BagsPrice bags_price;

    public FlightAverage(String name, String currency, long price_average, BagsPrice bags_price, String city) {
        this.name = name;
        this.city = city;
        this.currency = currency;
        this.price_average = price_average;
        this.bags_price = bags_price;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getPrice_average() {
        return price_average;
    }

    public void setPrice_average(long price_average) {
        this.price_average = price_average;
    }

    public BagsPrice getBags_price() {
        return bags_price;
    }

    public void setBags_price(BagsPrice bags_price) {
        this.bags_price = bags_price;
    }
}
