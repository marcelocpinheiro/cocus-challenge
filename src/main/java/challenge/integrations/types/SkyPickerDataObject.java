package challenge.integrations.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SkyPickerDataObject {
    private String cityFrom;
    private String cityTo;
    private Double price;

    @JsonProperty("bags_price")
    private JsonNode bagsPrice;

    public String getCityFrom() {
        return cityFrom;
    }

    public Double getBagPrice(int bagNumber) {
        if(this.bagsPrice != null && this.bagsPrice.has(String.valueOf(bagNumber))) {
            return this.bagsPrice.get(String.valueOf(bagNumber)).asDouble();
        }
        return 0.0;
    }

    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    public String getCityTo() {
        return cityTo;
    }

    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public JsonNode getBagsPrice() {
        return bagsPrice;
    }

    public void setBagsPrice(JsonNode bags_price) {
        this.bagsPrice = bags_price;
    }
}
