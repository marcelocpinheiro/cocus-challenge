package challenge.database.entities;

import javax.annotation.processing.Generated;
import javax.persistence.*;

@Entity
public class Airport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String iataCode;

    @Column(length = 1024)
    private String name;

    @Column(length = 1024)
    private String location;

    @Column(length = 1024)
    private String country;

    @Override
    public String toString() {
        return "Airport{" +
                "id=" + id +
                ", iataCode='" + iataCode + '\'' +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    public Airport(String iataCode, String name, String location, String country) {
        this.iataCode = iataCode;
        this.name = name;
        this.location = location;
        this.country = country;
    }

    public Airport(){ }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIataCode() {
        return iataCode;
    }

    public void setIataCode(String iataCode) {
        this.iataCode = iataCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
