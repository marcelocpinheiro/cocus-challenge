package challenge.services;

import challenge.controllers.FlightController;
import challenge.database.entities.Airport;
import challenge.database.repositories.AirportRepository;
import challenge.integrations.IntegrationInterface;
import challenge.integrations.types.SkyPickerDataObject;
import challenge.integrations.types.SkyPickerIntegrationParameters;
import challenge.integrations.types.SkyPickerResponse;
import challenge.models.BagsPrice;
import challenge.models.FlightAverage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Service
public class FlightService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FlightService.class);

    private IntegrationInterface integration;
    private AirportRepository airportRepository;

    public FlightService(@Qualifier("skypicker") IntegrationInterface integration, AirportRepository airportRepository) {
        this.integration = integration;
        this.airportRepository = airportRepository;
    }

    @Cacheable("requests")
    public FlightAverage process(String from, String to, Date dateFrom, Date dateTo, String curr) {

        LOGGER.info("Processing data: Airport from {} to {}, date  from {} to {}, currency {}", from, to, dateFrom, dateTo, curr);

        try {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            String formattedDateTo = null, formattedDateFrom = null;

            if(dateTo != null)  formattedDateTo = df.format(dateTo);
            if(dateFrom != null) formattedDateFrom = df.format(dateFrom);

            SkyPickerResponse response = (SkyPickerResponse) integration.callService(new SkyPickerIntegrationParameters(from, to, curr,  formattedDateFrom, formattedDateTo));

            LOGGER.info("Starting to calculate average data from response");

            String city = response.getCityTo();
            String name = "Unknown";

            List<Airport> airportList = airportRepository.findByIataCode(to);
            if(airportList.size() > 0) {
                name = airportList.get(0).getName();
            }

            Double priceAccumulator = 0.0;
            Double bag1Accumulator = 0.0;
            Double bag2Accumulator = 0.0;

            for(SkyPickerDataObject obj:response.getData()) {
                priceAccumulator += obj.getPrice();
                bag1Accumulator += obj.getBagPrice(1);
                bag2Accumulator += obj.getBagPrice(2);
            }

            int dataLength = response.getData().length;
            long averagePrice = Math.round(priceAccumulator / dataLength);
            long averageBag1Price = Math.round(bag1Accumulator / dataLength);
            long averageBag2Price = Math.round(bag2Accumulator / dataLength);
            BagsPrice bp = new BagsPrice(averageBag1Price, averageBag2Price);

            LOGGER.info("Prices calculated (Airport from {} to {}, date  from {} to {}, currency {}) . Flight average price: {} | Bag 1 average price: {} | Bag 2 average price: {}",
                    from, to, dateFrom, dateTo, curr, averagePrice, averageBag1Price, averageBag2Price);

            return new FlightAverage(name, curr, averagePrice, bp, city);
        }catch (Exception e) {
            LOGGER.error("Error fallback. Returning empty response.");
            return new FlightAverage("Unknown", curr, 0, null, "Unknown");
        }
    }
}
