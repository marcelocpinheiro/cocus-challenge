package challenge.controllers;

import challenge.annotations.AirportCodeAnnotation;
import challenge.models.FlightAverage;
import challenge.services.FlightService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.*;

@RestController
@Validated
@RequestMapping("flight")
public class FlightController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FlightController.class);

    private final FlightService service;

    public FlightController(FlightService service) {
        this.service = service;
    }

    @GetMapping( value = "/average",  produces = "application/json", consumes = "application/json")
    @AirportCodeAnnotation // There's some validation on this annotation, check it out :D
    public HashMap<String, Object> getFlightAverage(@RequestParam  String[] dest,
                                                    @RequestParam String from,
                                                    @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") @Valid Date dateFrom,
                                                    @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") @Valid Date dateTo,
                                                    @RequestParam(defaultValue = "EUR") @Pattern(regexp = "[A-Z]{3}") String curr,
                                                    HttpServletRequest request) {

       LOGGER.info("Request received on \"/average\" endpoint. Complete request:  {}", request.getQueryString());

       HashMap<String, Object> response = new HashMap<>();
        for (String destination: dest) {
            response.put(destination, service.process(from, destination, dateFrom, dateTo, curr));
        }

        return response;
    }
}
